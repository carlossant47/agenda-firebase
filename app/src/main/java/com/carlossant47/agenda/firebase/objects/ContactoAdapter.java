package com.carlossant47.agenda.firebase.objects;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.carlossant47.agenda.firebase.R;


import java.util.ArrayList;

public class ContactoAdapter extends ArrayAdapter<Contacto> {
    private Context context;
    private int textViewResourceID;
    private ArrayList<Contacto> contactos;
    private LayoutInflater inflater;

    public ContactoAdapter(Context context, @IdRes int idTextView, @LayoutRes int  layoutIdResource, ArrayList<Contacto> items)
    {
        super(context, idTextView, items);
        this.context = context;
        this.textViewResourceID = layoutIdResource;
        this.contactos = items;
        this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = this.inflater.inflate(this.textViewResourceID, parent, false);
        TextView lbNombre  = view.findViewById(R.id.lbNombre);
        TextView lbTelefono  = view.findViewById(R.id.lbTelefono);
        ImageButton btFavorite = view.findViewById(R.id.btnFavorite);

        btFavorite.setImageResource(contactos.get(position).isFavorito() ?
                R.drawable.ic_start_checked : R.drawable.ic_start);
        lbNombre.setText(contactos.get(position).getNombre());
        lbTelefono.setText(contactos.get(position).getTelefono1());

        return view;


    }
    public View getDropDownView(int position, View convertView, ViewGroup parent)
    {
        return getView(position, convertView, parent);
    }
}
