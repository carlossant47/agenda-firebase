package com.carlossant47.agenda.firebase;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.carlossant47.agenda.firebase.objects.Contacto;
import com.carlossant47.agenda.firebase.objects.FirebaseReferences;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private FirebaseDatabase basedatabase;
    private DatabaseReference referencia;
    private Button btnNuevo;
    private ListView listContactos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        basedatabase = FirebaseDatabase.getInstance();
        referencia = basedatabase.getReferenceFromUrl(FirebaseReferences.URL_DATABASE
                + FirebaseReferences.DATABASE_NAME + "/" +
                FirebaseReferences.TABLE_NAME);
        btnNuevo = findViewById(R.id.btNuevo);
        listContactos = findViewById(R.id.listContactos);
        consultContacts();
    }

    public void consultContacts(){
        final ArrayList<Contacto> contactos = new ArrayList<>();
        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Contacto contacto = dataSnapshot.getValue(Contacto.class);
                contactos.add(contacto);
                final ContactoAdapter adapter = new ContactoAdapter(getApplicationContext(), R.id.lbNombre,
                        R.layout.contacto_layout, contactos);
                listContactos.setAdapter(adapter);
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        referencia.addChildEventListener(listener);
    }

    public class ContactoAdapter extends ArrayAdapter<Contacto> {
        private Context context;
        private int textViewResourceID;
        private ArrayList<Contacto> contactos;
        private LayoutInflater inflater;

        public ContactoAdapter(Context context, @IdRes int idTextView, @LayoutRes int  layoutIdResource, ArrayList<Contacto> items)
        {
            super(context, idTextView, items);
            this.context = context;
            this.textViewResourceID = layoutIdResource;
            this.contactos = items;
            this.inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = this.inflater.inflate(this.textViewResourceID, parent, false);
            TextView lbNombre  = view.findViewById(R.id.lbNombre);
            TextView lbTelefono  = view.findViewById(R.id.lbTelefono);
            ImageButton btFavorite = view.findViewById(R.id.btnFavorite);
            ImageButton btnDelete = view.findViewById(R.id.btnEliminar);
            ImageButton btnEditar = view.findViewById(R.id.btnEditar);
            btFavorite.setImageResource(contactos.get(position).isFavorito() ?
                    R.drawable.ic_start_checked : R.drawable.ic_start);
            lbNombre.setText(contactos.get(position).getNombre());
            lbTelefono.setText(contactos.get(position).getTelefono1());
            btnEditar.setOnClickListener(this.btnEditarAction(position));
            return view;


        }

        private View.OnClickListener btnEditarAction(int pos)
        {
            return v -> {
                Bundle oBundle = new Bundle();
                oBundle.putSerializable("contacto", contactos.get(pos));
                Intent i = new Intent();
                i.putExtras(oBundle);
                setResult(Activity.RESULT_OK, i);
                finish();
            };
        }
        public View.OnClickListener btnEliminarAction(int pos)
        {
            return v -> {
                deleteContactos(contactos.get(pos).getId());
                contactos.remove(pos);
                notifyDataSetChanged();
                Toast.makeText(context, "Se elimino con exito", Toast.LENGTH_SHORT).show();
            };
        }
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            return getView(position, convertView, parent);
        }
    }

    public void deleteContactos(String childIndex){
        referencia.child(String.valueOf(childIndex)).removeValue();
    }

}
