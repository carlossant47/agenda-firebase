package com.carlossant47.agenda.firebase.objects;

public class FirebaseReferences {
    final static public String URL_DATABASE = "//agenda-91331.firebaseio.com/";
    final static public String DATABASE_NAME = "agenda";
    final static public String TABLE_NAME = "contactos";
}
