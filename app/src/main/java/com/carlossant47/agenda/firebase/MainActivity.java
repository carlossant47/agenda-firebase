package com.carlossant47.agenda.firebase;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.carlossant47.agenda.firebase.objects.Contacto;
import com.carlossant47.agenda.firebase.objects.FirebaseReferences;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private EditText txtNombre;
    private EditText txtTel1;
    private EditText txtTel2;
    private EditText txtDomicilio;
    private EditText txtNotas;
    private CheckBox cbFavorito;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnLista;
    private Button btnSalir;
    private Contacto saveContact = null;
    private boolean isEdit = false;
    private FirebaseDatabase basedatabase;
    private DatabaseReference referencia;
    private String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        btnGuardar.setOnClickListener(this::btnGuardarAction);
        btnLimpiar.setOnClickListener(v-> limpiar());
        btnSalir.setOnClickListener(v -> finish());
        btnLista.setOnClickListener(this::btnListarAction);

    }

    private void btnListarAction(View view) {
        if(!isNetworkAvailable())
        {
            Toast.makeText(getApplicationContext(), "Se requiere conexion a internet",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(this, ListActivity.class);
        startActivity(intent);
    }


    private void btnGuardarAction(View view) {
        if (!validar())
        {
            Toast.makeText(getApplicationContext(), "Error no puedes dejar los campos vacios", Toast.LENGTH_SHORT).show();
            return;
        }

        if(!isNetworkAvailable())
        {
            Toast.makeText(getApplicationContext(), "Se requiere conexion a internet", Toast.LENGTH_SHORT).show();
            return;
        }
        Contacto contacto = new Contacto();
        contacto.setNombre(getText(txtNombre));
        contacto.setDomicilio(getText(txtDomicilio));
        contacto.setNotas(getText(txtNotas));
        contacto.setTelefono1(getText(txtTel1));
        contacto.setTelefono2(getText(txtTel2));
        contacto.setFavorito(cbFavorito.isChecked());
        if(isEdit){
            insertContacto(contacto);
            Toast.makeText(getApplicationContext(),
                    "Contacto guardado con exito",
                    Toast.LENGTH_SHORT).show();
        }
        else{
            actualizarContacto(
                    id,contacto);
            Toast.makeText(getApplicationContext(),
                    "Contacto actualizado con exito",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public void insertContacto(Contacto c) {
        DatabaseReference newContactoReference = referencia.push();

        String id = newContactoReference.getKey();
        c.setId(id);
        newContactoReference.setValue(c);
    }

    public void actualizarContacto(String id, Contacto contacto) {

        contacto.setId(id);
        referencia.child(String.valueOf(id)).setValue(contacto);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    private void init()
    {
        this.basedatabase = FirebaseDatabase.getInstance();
        this.referencia = this.basedatabase.getReferenceFromUrl(FirebaseReferences.URL_DATABASE
                + FirebaseReferences.DATABASE_NAME + "/" +
                FirebaseReferences.TABLE_NAME);
        txtNombre = findViewById(R.id.txtNombre);
        txtTel1 = findViewById(R.id.txtTel1);
        txtTel2 = findViewById(R.id.txtTel2);
        txtDomicilio = findViewById(R.id.txtDomicilio);
        txtNotas = findViewById(R.id.txtNotas);
        cbFavorito = findViewById(R.id.cbFavorito);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLista = findViewById(R.id.btnLista);
        btnSalir = findViewById(R.id.btnSalir);

    }

    private void limpiar(){
        txtDomicilio.setText("");
        txtNombre.setText("");
        txtTel1.setText("");
        txtTel2.setText("");
        txtNotas.setText("");
        cbFavorito.setChecked(false);
        isEdit = false;
        saveContact = null;

    }


    private boolean validar(){
        if(validarCampo(txtDomicilio) || validarCampo(txtNombre) ||
                validarCampo(txtNotas) || validarCampo(txtTel1) || validarCampo(txtTel2))
            return false;
        else
            return true;
    }

    private boolean validarCampo(EditText txt)
    {
        return txt.getText().toString().matches("");
    }

    private String getText(EditText txt)
    {
        return txt.getText().toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null) {
            Bundle oBundle = intent.getExtras();
            if (Activity.RESULT_OK == resultCode) {
                Contacto contacto = (Contacto) oBundle.getSerializable("contacto");
                saveContact = contacto;
                id = contacto.getId();
                txtNombre.setText(contacto.getNombre());
                txtTel1.setText(contacto.getTelefono1());
                txtTel2.setText(contacto.getTelefono2());
                txtDomicilio.setText(contacto.getDomicilio());
                txtNotas.setText(contacto.getNotas());
                if (contacto.isFavorito()) {
                    cbFavorito.setChecked(true);
                }
            } else {
                limpiar();
            }
        }
    }
}
